#!/usr/bin/env sh
for i in $(eval echo "$(flatpak-spawn --host ls /usr/bin/ | grep xdg- | tr '\n' ' ')"); do
        alias "$i=flatpak-spawn --host $i"
done